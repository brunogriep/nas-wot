import numpy as np
import torch




def hooklogdet(K, labels=None):
    s, ld = np.linalg.slogdet(K)
    return ld

def hookeigen(K, labels=None):
    w, v = np.linalg.eig(K)
    return [w, v]

def hooksvd(K, labels=None):
    u, s, vh = np.linalg.svd(K)
    return [u, s, vh]

def hooktrace(K, labels=None):
    return np.trace(K)

def random_score(jacob, label=None):
    return np.random.normal()


_scores = {
        'hook_logdet': hooklogdet,
        'random': random_score,
        'hook_svd': hooksvd,
        'hook_trace': hooktrace,
        'hook_eigen': hookeigen
        }

def get_score_func(score_name):
    return _scores[score_name]

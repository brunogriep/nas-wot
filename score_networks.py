import argparse
import nasspace
import datasets
import random
import numpy as np
import torch
import os
import time
from scores import get_score_func
from scipy import stats
from pycls.models.nas.nas import Cell
from utils import add_dropout, init_network
from tqdm import tqdm

parser = argparse.ArgumentParser(description='NAS Without Training')
parser.add_argument('--data_loc',
                    default='./datasets/cifar10',
                    type=str,
                    help='dataset folder')
parser.add_argument('--api_loc',
                    default='./datasets/NATS-sss-v1_0-50262-simple/',
                    type=str,
                    help='path to API')
parser.add_argument('--save_loc',
                    default='results',
                    type=str,
                    help='folder to save results')
parser.add_argument('--save_string',
                    default='naswot',
                    type=str,
                    help='prefix of results file')
parser.add_argument('--score',
                    default='hook_logdet',
                    type=str,
                    help='the score to evaluate')
parser.add_argument('--nasspace',
                    default='nasbench201',
                    type=str,
                    help='the nas search space to use')
parser.add_argument('--batch_size', default=128, type=int)
parser.add_argument('--repeat',
                    default=1,
                    type=int,
                    help='how often to repeat a single image with a batch')
parser.add_argument('--augtype',
                    default='none',
                    type=str,
                    help='which perturbations to use')
parser.add_argument('--sigma',
                    default=0.05,
                    type=float,
                    help='noise level if augtype is "gaussnoise"')
parser.add_argument('--GPU', default='0', type=str)
parser.add_argument('--seed', default=1, type=int)
parser.add_argument('--init', default='', type=str)
parser.add_argument('--trainval', action='store_true')
parser.add_argument('--dropout', action='store_true')
parser.add_argument('--dataset', default='cifar10', type=str)
parser.add_argument('--score_type', default='log/mean', type=str)
parser.add_argument(
    '--maxofn',
    default=1,
    type=int,
    help='score is the max of this many evaluations of the network')
parser.add_argument('--n_samples', default=100, type=int)
parser.add_argument('--n_runs', default=500, type=int)
parser.add_argument('--stem_out_channels',
                    default=16,
                    type=int,
                    help='output channels of stem convolution (nasbench101)')
parser.add_argument('--num_stacks',
                    default=3,
                    type=int,
                    help='#stacks of modules (nasbench101)')
parser.add_argument('--num_modules_per_stack',
                    default=3,
                    type=int,
                    help='#modules per stack (nasbench101)')
parser.add_argument('--num_labels',
                    default=1,
                    type=int,
                    help='#classes (nasbench101)')
parser.add_argument('--num_iter',
                    default=100,
                    type=int)

args = parser.parse_args()
os.environ['CUDA_VISIBLE_DEVICES'] = args.GPU

# Reproducibility
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)


def get_batch_jacobian(net, x, target, device, args=None):
  net.zero_grad()
  x.requires_grad_(True)
  y, out = net(x)
  y.backward(torch.ones_like(y))
  jacob = x.grad.detach()
  return jacob, target.detach(), y.detach(), out.detach()


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
savedataset = args.dataset
dataset = 'fake' if 'fake' in args.dataset else args.dataset
args.dataset = args.dataset.replace('fake', '')
if args.dataset == 'cifar10':
  args.dataset = args.dataset + '-valid'
searchspace = nasspace.get_search_space(args)
if 'valid' in args.dataset:
  args.dataset = args.dataset.replace('-valid', '')
train_loader = datasets.get_data(args.dataset, args.data_loc, args.trainval,
                                 args.batch_size, args.augtype, args.repeat,
                                 args)
os.makedirs(args.save_loc, exist_ok=True)

if "sss" in args.api_loc:
  result_search_space = "sss"
else:
  result_search_space = "tss"

filename = f'{args.save_loc}/{args.save_string}_{result_search_space}_{args.score}_{args.nasspace}_{savedataset}{"_" + args.init + "_" if args.init != "" else args.init}_{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
accfilename = f'{args.save_loc}/{args.save_string}_{result_search_space}_accs_{args.nasspace}_{savedataset}_{args.trainval}'
sizefilename = f'{args.save_loc}/{args.save_string}_{result_search_space}_sizes_{args.nasspace}_{savedataset}_{args.trainval}'

if args.dataset == 'cifar10':
  acc_type = 'ori-test'
  val_acc_type = 'x-valid'
else:
  acc_type = 'x-test'
  val_acc_type = 'x-valid'

sizes = np.zeros(args.num_iter)
scores = np.zeros(args.num_iter)
accs = np.zeros(args.num_iter)
exec_times = np.zeros(args.num_iter)

for i, (uid, network) in enumerate(searchspace):
  print(i,"/",args.num_iter)
  if i == args.num_iter:
    break;
  # Reproducibility
  try:
    start = time.time()

    if args.dropout:
      add_dropout(network, args.sigma)
    if args.init != '':
      init_network(network, args.init)
    if 'hook_' in args.score:
      network.K = np.zeros((args.batch_size, args.batch_size))

      def counting_forward_hook(module, inp, out):
        try:
          if not module.visited_backwards:
            return
          if isinstance(inp, tuple):
            inp = inp[0]
          inp = inp.view(inp.size(0), -1)
          x = (inp > 0).float()
          K = x @ x.t()
          K2 = (1. - x) @ (1. - x.t())
          network.K = network.K + K.cpu().numpy() + K2.cpu().numpy()
        except:
          pass

      def counting_backward_hook(module, inp, out):
        module.visited_backwards = True

      for name, module in network.named_modules():
        if 'ReLU' in str(type(module)):
          module.register_forward_hook(counting_forward_hook)
          module.register_backward_hook(counting_backward_hook)

    network = network.to(device)
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    
    s = []

    for j in range(args.maxofn):
      data_iterator = iter(train_loader)
      x, target = next(data_iterator)
      x2 = torch.clone(x)
      x2 = x2.to(device)
      x, target = x.to(device), target.to(device)
      jacobs, labels, y, out = get_batch_jacobian(network, x, target, device, args)

      if 'hook_logdet' in args.score:
        network(x2.to(device))
        deter = get_score_func('hook_logdet')(network.K, target)
        s.append(deter)
      elif 'hook_eigen' in args.score:
        network(x2.to(device))
        eig_values, eig_vectors = get_score_func(args.score)(network.K, target)
        s.append(eig_values)
        network(x2.to(device))
        svd_u, svd_s, svd_vh = get_score_func('hook_svd')(network.K, target)
        s.append(svd_s)
      elif 'hook_trace' in args.score:
        network(x2.to(device))
        trace = np.trace(network.K)
      else:
        s.append(get_score_func(args.score)(jacobs, labels))

    
    if 'sum' in args.score_type:
      if 'log' in args.score_type:
        scores[i] = np.log(np.sum(s))
      else:
        scores[i] = np.sum(s)
    elif 'max_min' in args.score_type:
      if 'log' in args.score_type:
        scores[i] = np.log(np.max(s)/np.min(s))
      else:
        scores[i] = np.max(s)/np.min(s)
    elif 'mean' in args.score_type:
      if 'log' in args.score_type:
        scores[i] = np.log(np.mean(s))
      else:
        scores[i] = np.mean(s)
    elif 'trace' in args.score:
      if 'log' in args.score_type:
        scores[i] = np.log(trace)
      else:
        scores[i] = trace
    else:
      scores[i] = np.mean(s)

    exec_times[i] = (time.time() - start)

    accs[i] = searchspace.get_final_accuracy(uid, acc_type, args.trainval)
    if args.nasspace == "nasbench201": 
      sizes[i] = searchspace.get_network_size_in_mb()
    if args.nasspace == "nasbench101": 
      sizes[i] = searchspace.get_network_size_in_mb(uid)
    if "nds" in args.nasspace: 
      sizes[i] = searchspace.get_network_size_in_mb(uid)

    accs_ = accs[~np.isnan(scores)]
    scores_ = scores[~np.isnan(scores)]


    numnan = np.isnan(scores).sum()
    tau, p = stats.kendalltau(accs_[:max(i - numnan, 1)],
                              scores_[:max(i - numnan, 1)])

    # print(f'Kendal Tau: {tau}')
  except Exception as e:
    print(e)
    accs[i] = searchspace.get_final_accuracy(uid, acc_type, args.trainval)
    scores[i] = np.nan
    if args.nasspace == "nasbench201": 
      sizes[i] = searchspace.get_network_size_in_mb()
    if args.nasspace == "nasbench101": 
      sizes[i] = searchspace.get_network_size_in_mb(uid)
    if "nds" in args.nasspace: 
      sizes[i] = searchspace.get_network_size_in_mb(uid)

if 'eigen' in args.score:
  save_string = "eig"
elif 'svd' in args.score:
  save_string = "svd"
elif 'trace' in args.score:
  save_string = "trace"
else:
  save_string = args.score

my_dict = { 'Scores': scores, 'Accs': accs, 'Sizes': sizes, 'ExecTime': exec_times}
filename = f'{args.save_loc}/{save_string}/{args.score_type}/{result_search_space}_{args.nasspace}_{savedataset}{"_" + args.init + "_" if args.init != "" else args.init}_{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
print(filename)

np.save(filename, my_dict)


import argparse
import pandas as pd
import tabulate
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description='NAS Without Training')
parser.add_argument('--data_loc', default='../cifardata/', type=str, help='dataset folder')
parser.add_argument('--api_loc', default='../NAS-Bench-201-v1_0-e61699.pth',
                    type=str, help='path to API')
parser.add_argument('--save_loc', default='results', type=str, help='folder to save results')
parser.add_argument('--npy_loc', default='results', type=str, help='folder to save results')
parser.add_argument('--save_string', default='naswot', type=str, help='prefix of results file')
parser.add_argument('--score', default='hook_logdet', type=str, help='the score to evaluate')
parser.add_argument('--nasspace', default='nasbench201', type=str, help='the nas search space to use')
parser.add_argument('--batch_size', default=128, type=int)
parser.add_argument('--repeat', default=1, type=int, help='how often to repeat a single image with a batch')
parser.add_argument('--augtype', default='none', type=str, help='which perturbations to use')
parser.add_argument('--sigma', default=0.05, type=float, help='noise level if augtype is "gaussnoise"')
parser.add_argument('--init', default='', type=str)
parser.add_argument('--GPU', default='0', type=str)
parser.add_argument('--seed', default=1, type=int)
parser.add_argument('--trainval', default='True', type=str)
parser.add_argument('--dropout', action='store_true')
parser.add_argument('--dataset', default='cifar10', type=str)
parser.add_argument('--maxofn', default=1, type=int, help='score is the max of this many evaluations of the network')
parser.add_argument('--n_samples', default=10, type=int)
parser.add_argument('--n_runs', default=500, type=int)
parser.add_argument('--stem_out_channels', default=16, type=int, help='output channels of stem convolution (nasbench101)')
parser.add_argument('--num_stacks', default=3, type=int, help='#stacks of modules (nasbench101)')
parser.add_argument('--num_modules_per_stack', default=3, type=int, help='#modules per stack (nasbench101)')
parser.add_argument('--num_labels', default=1, type=int, help='#classes (nasbench101)')
parser.add_argument('--plot_size', default='', type=str)
parser.add_argument('--sss_tss', default='sss', type=str)
parser.add_argument('--scale_func', default='inverse', type=str)
parser.add_argument('--flash_max', default=0.440, type=float, help='value in megabytes')
parser.add_argument('--ram_max', default=0.520, type=float, help='value in megabytes')
parser.add_argument('--apply_esp32', default=True, type=bool)

args = parser.parse_args()

if args.sss_tss == "sss":
  result_search_space = "sss"
else:
  result_search_space = "tss"

scaled_filename = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_scaled_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
scores_filename = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_original_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
accfilename = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_accs_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
sizefilename = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_sizes_{args.nasspace}_{args.dataset}_{args.trainval}.npy'


scores = np.load(scores_filename)
scaled_scores = np.load(scaled_filename)
accs = np.load(accfilename)
sizes = np.load(sizefilename)

scaled_scores = scaled_scores / np.max(scores)

nasspacenames = {
    'nds_resnext-a_in': 'NDS-ResNeXt-A(ImageNet)',
    'nds_resnext-b_in': 'NDS-ResNeXt-B(ImageNet)',
    'nds_resnext-a': 'NDS-ResNeXt-A(CIFAR10)',
    'nds_resnext-b': 'NDS-ResNeXt-B(CIFAR10)',
    'nds_nasnet': 'NDS-NASNet(CIFAR10)',
    'nds_nasnet_in': 'NDS-NASNet(ImageNet)',
    'nds_enas': 'NDS-ENAS(CIFAR10)',
    'nds_enas_in': 'NDS-ENAS(ImageNet)',
    'nds_amoeba': 'NDS-Amoeba(CIFAR10)',
    'nds_amoeba_in': 'NDS-Amoeba(ImageNet)',
    'nds_resnet': 'NDS-ResNet(CIFAR10)',
    'nds_pnas': 'NDS-PNAS(CIFAR10)',
    'nds_pnas_in': 'NDS-PNAS(ImageNet)',
    'nds_darts': 'NDS-DARTS(CIFAR10)',
    'nds_darts_in': 'NDS-DARTS(ImageNet)',
    'nds_darts_fix-w-d': 'NDS-DARTS fixed width/depth (CIFAR10)',
    'nds_darts_in_fix-w-d': 'NDS-DARTS fixed width/depth (ImageNet)',
    'nds_darts_in': 'NDS-DARTS(ImageNet)',
    'nasbench101': 'NAS-Bench-101',
    'nasbench201': 'NAS-Bench-201'
}

if args.plot_size == '0':
    file_begining_name = 'accs'
elif args.plot_size == '2':
    file_begining_name = 'AS'
else:
    file_begining_name = 'sizes'
    
if args.scale_func != '':
    table_filename = f'{args.save_loc}/table/{args.scale_func}_sigma{args.flash_max}MB_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
    table_filename_csv = f'{args.save_loc}/table/csv/{args.scale_func}_sigma{args.flash_max}MB_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
else:
    table_filename = f'{args.save_loc}/table/{args.scale_func}_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
    table_filename_csv = f'{args.save_loc}/table/csv/{args.scale_func}_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'

sorted_index_array = np.argsort(scaled_scores)
sorted_index_array = sorted_index_array[::-1]

sorted_scores = scores[sorted_index_array]
sorted_scaled_scores = scaled_scores[sorted_index_array]
sorted_accs = accs[sorted_index_array]
sorted_sizes = sizes[sorted_index_array]
  
rst_scores = sorted_scores[:args.n_samples]
rst_scaled_scores = sorted_scaled_scores[:args.n_samples]
rst_accs = sorted_accs[:args.n_samples]
rst_sizes = sorted_sizes[:args.n_samples]

df = []
for i in sorted_index_array[:args.n_samples]:
    rounded_scaled = float("{:.4f}".format(scaled_scores[i]))
    rounded_score = float("{:.2f}".format(scores[i]))
    rounded_acc = float("{:.2f}".format(accs[i]))
    rounded_size = float("{:.2f}".format(sizes[i]))
    df.append([rounded_scaled, rounded_score, rounded_acc, rounded_size])

df = pd.DataFrame(df, columns=[f'{args.scale_func} Scores','Original Scores',  'Accuracies (%)', 'Sizes (MB)' ])

df.to_csv(f'{table_filename_csv}.csv', index_label=f'{nasspacenames[args.nasspace]} {args.dataset} \n sigma={args.flash_max} MB \n Scaled with {args.scale_func} function')

print(table_filename)

if args.n_samples < 20:
    tabulate.tabulate(df.values,df.columns, tablefmt="pipe")
    fig, ax = plt.subplots()

    fig.patch.set_visible(False)
    ax.axis('off')
    ax.axis('tight')

    ax.table(cellText = df.values,
            rowLabels = df.index,
            colLabels = df.columns,
            loc = "center"
            )
    ax.set_title(f'{nasspacenames[args.nasspace]} {args.dataset} \n sigma={args.flash_max} MB \n Scaled with {args.scale_func} function')

    plt.savefig(table_filename + '.pdf')

from matplotlib.colors import hsv_to_rgb
import seaborn as sns
from scipy import stats
from scipy.special import logit, expit
from decimal import Decimal
import argparse
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mp
import matplotlib
matplotlib.use('Agg')

'''
font = {
        'size'   : 18}

matplotlib.rc('font', **font)
'''
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 14

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

parser = argparse.ArgumentParser(description='NAS Without Training')
parser.add_argument('--data_loc', default='../cifardata/',
                    type=str, help='dataset folder')
parser.add_argument('--api_loc', default='../NAS-Bench-201-v1_0-e61699.pth',
                    type=str, help='path to API')
parser.add_argument('--save_loc', default='results',
                    type=str, help='folder to save results')
parser.add_argument('--npy_loc', default='results',
                    type=str, help='folder to save results')
parser.add_argument('--save_string', default='naswot',
                    type=str, help='prefix of results file')
parser.add_argument('--score', default='hook_logdet',
                    type=str, help='the score to evaluate')
parser.add_argument('--nasspace', default='nasbench201',
                    type=str, help='the nas search space to use')
parser.add_argument('--batch_size', default=128, type=int)
parser.add_argument('--repeat', default=1, type=int,
                    help='how often to repeat a single image with a batch')
parser.add_argument('--augtype', default='none', type=str,
                    help='which perturbations to use')
parser.add_argument('--sigma', default=0.05, type=float,
                    help='noise level if augtype is "gaussnoise"')
parser.add_argument('--init', default='', type=str)
parser.add_argument('--GPU', default='0', type=str)
parser.add_argument('--seed', default=1, type=int)
parser.add_argument('--trainval', default='True', type=str)
# parser.add_argument('--trainval', action='store_true')
parser.add_argument('--dropout', action='store_true')
parser.add_argument('--dataset', default='cifar10', type=str)
parser.add_argument('--maxofn', default=1, type=int,
                    help='score is the max of this many evaluations of the network')
parser.add_argument('--n_samples', default=100, type=int)
parser.add_argument('--n_runs', default=500, type=int)
parser.add_argument('--stem_out_channels', default=16, type=int,
                    help='output channels of stem convolution (nasbench101)')
parser.add_argument('--num_stacks', default=3, type=int,
                    help='#stacks of modules (nasbench101)')
parser.add_argument('--num_modules_per_stack', default=3,
                    type=int, help='#modules per stack (nasbench101)')
parser.add_argument('--num_labels', default=1, type=int,
                    help='#classes (nasbench101)')
parser.add_argument('--plot_size', default='', type=str)
parser.add_argument('--sss_tss', default='sss', type=str)
parser.add_argument('--scale_func', default='inverse', type=str)
parser.add_argument('--flash_max', default=0.440,
                    type=float, help='value in megabytes')
parser.add_argument('--ram_max', default=0.520,
                    type=float, help='value in megabytes')
parser.add_argument('--apply_esp32', default=True, type=bool)


args = parser.parse_args()

if args.sss_tss == "sss":
    result_search_space = "sss"
else:
    result_search_space = "tss"

random.seed(args.seed)
np.random.seed(args.seed)

filename = f'{args.npy_loc}/{args.save_string}_{result_search_space}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}_{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}.npy'
accfilename = f'{args.npy_loc}/{args.save_string}_{result_search_space}_accs_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
sizefilename = f'{args.npy_loc}/{args.save_string}_{result_search_space}_sizes_{args.nasspace}_{args.dataset}_{args.trainval}.npy'

scores = np.load(filename)
accs = np.load(accfilename)
sizes = np.load(sizefilename)


def make_colours_by_hue(h, v=1.):
    return [hsv_to_rgb((h1 if h1 < 1. else h1-1., s, v)) for h1, s, v in zip(np.linspace(h, h+0.05, 5), np.linspace(1., .6, 5), np.linspace(0.1, 1., 5))]


def make_colours(cols): return [mp.colors.to_rgba(c) for c in cols]


oranges = make_colours(['#811F41', '#A92941', '#D15141', '#EF7941', '#F99C4B'])
blues = make_colours(['#190C30', '#241147', '#34208C', '#4882FA', '#81BAFC'])
if args.nasspace == 'nasbench101':
    colours = make_colours_by_hue(0.9)
elif 'darts' in args.nasspace:
    colours = make_colours_by_hue(0.0)
elif 'pnas' in args.nasspace:
    colours = make_colours_by_hue(0.1)
elif args.nasspace == 'nasbench201':
    colours = make_colours_by_hue(0.3)
elif 'enas' in args.nasspace:
    colours = make_colours_by_hue(0.4)
elif 'resnet' in args.nasspace:
    colours = make_colours_by_hue(0.5)
elif 'amoeba' in args.nasspace:
    colours = make_colours_by_hue(0.6)
elif 'nasnet' in args.nasspace:
    colours = make_colours_by_hue(0.7)
elif 'resnext-b' in args.nasspace:
    colours = make_colours_by_hue(0.8)
else:
    from zlib import crc32

    def bytes_to_float(b):
        return float(crc32(b) & 0xffffffff) / 2**32

    def str_to_float(s, encoding="utf-8"):
        return bytes_to_float(s.encode(encoding))
    colours = make_colours_by_hue(str_to_float(args.nasspace))


def make_colordict(colours, points):
    cdict = {'red': [[pt, colour[0], colour[0]] for pt, colour in zip(points, colours)],
             'green': [[pt, colour[1], colour[1]] for pt, colour in zip(points, colours)],
             'blue': [[pt, colour[2], colour[2]] for pt, colour in zip(points, colours)]}
    return cdict


def make_colormap(dataset, space, colours):
    if dataset == 'cifar10' and 'resn' in space:
        points = [0., 0.85, 0.9, 0.95, 1.0, 1.0]
        colours = [colours[0]] + colours
    elif dataset == 'cifar10' and 'nds_darts' in space:
        points = [0., 0.8, 0.85, 0.9, 0.95, 1.0]
        colours = [colours[0]] + colours
    elif dataset == 'cifar10' and 'pnas' in space:
        points = [0., 0.875, 0.9, 0.925, 0.95, 1.0]
        colours = [colours[0]] + colours
    elif dataset == 'cifar10':
        points = [0., 0.6, 0.7, 0.8, 0.9, 1.0]
        colours = [colours[0]] + colours
    elif dataset == 'cifar100':
        points = [0., 0.3, 0.4, 0.5, 0.6, 0.7, 1.0]
        colours = [colours[0]] + colours + [colours[-1]]
    else:
        points = [0., 0.1, 0.2, 0.3, 0.4, 1.0]
        colours = colours + [colours[-1]]

    cdict = make_colordict(colours, points)
    return cdict


cdict = make_colormap(args.dataset, args.nasspace, colours)
newcmp = mp.colors.LinearSegmentedColormap(
    'testCmap', segmentdata=cdict, N=256)

if args.nasspace == 'nasbench101':
    accs = accs[:10000]
    scores = scores[:10000]
    inds = accs > 0.5
    accs = accs[inds]
    scores = scores[inds]
elif args.nasspace == 'nds_amoeba' or args.nasspace == 'nds_darts_fix-w-d':
    inds = accs > 15.
    accs = accs[inds]
    scores = scores[inds]
elif args.nasspace == 'nds_darts':
    inds = accs > 15.
    from nasspace import get_search_space
    searchspace = get_search_space(args)
    accs = accs[inds]
    scores = scores[inds]
else:
    inds = accs > 15.
    accs = accs[inds]
    sizes = sizes[inds]
    scores = scores[inds]

inds = scores == 0.
accs = accs[~inds]
scores = scores[~inds]
sizes = sizes[~inds]


if accs.size > 1000:
    inds = np.random.choice(accs.size, 1000, replace=False)
    accs = accs[inds]
    sizes = sizes[inds]
    scores = scores[inds]

inds = np.isnan(scores)
accs = accs[~inds]
sizes = sizes[~inds]
scores = scores[~inds]
scaled_scores = np.copy(scores)

tau, p = stats.kendalltau(accs, scores)

# *************************************
# *************************************
# *************************************

if args.scale_func == 'reciprocal_flash_max':
    sizes = sizes + args.flash_max
    
    for i in range(len(sizes)):
        if sizes[i] >= args.flash_max:
            scaled_scores[i] = scaled_scores[i] / sizes[i]
        else:
          scaled_scores[i] = scores[i]

    # scaled_scores = scaled_scores / sizes
    corrector = np.max(scores)/np.max(scaled_scores)
if args.scale_func == 'reciprocal':
    sizes = sizes + 1e-6
    scaled_scores = scaled_scores / sizes
    corrector = np.max(scores)/np.max(scaled_scores)
elif args.scale_func == 'cosh':
    if args.apply_esp32 == True:
        mu = args.flash_max
    else:
        mu = np.mean(sizes)
    scaled_scores = scaled_scores * 0.5 * np.cosh(0.5 * np.pi * (sizes - mu))**-2
    corrector = np.max(scores)/np.max(scaled_scores)
elif args.scale_func == 'cosh_threshold':
    mu = args.flash_max
    for i in range(len(sizes)):
        if sizes[i] >= mu:
            scaled_scores[i] = scaled_scores[i] * 0.5 * np.cosh(0.5 * np.pi * (sizes[i] - mu))**-2
        else:
          scaled_scores[i] = scores[i]
    corrector = 1
elif args.scale_func == 'gaussian':
    if args.apply_esp32 == True:
        mu = args.flash_max
        sig = 2 * mu
    else:
        mu = np.mean(sizes)
        sig = 1.5 * np.mean(sizes)
    exp_term = -0.5 * np.power((sizes - mu) / sig, 2)
    scaled_scores = scaled_scores * (np.exp(exp_term) / (sig * np.sqrt(2 * np.pi)))
    corrector = np.max(scores)/np.max(scaled_scores)
elif args.scale_func == 'gaussian_threshold':
    mu = args.flash_max
    sig = 2 * mu
    for i in range(len(sizes)):
        if sizes[i] >= sig:
            exp_term = -0.5 * np.power((sizes[i] - mu) / sig, 2)
            scaled_scores[i] = scaled_scores[i] * (np.exp(exp_term) / (sig * np.sqrt(2 * np.pi)))
        else:
          scaled_scores[i] = scores[i]
    corrector = 1
elif args.scale_func == 'rayleigh':
    if args.apply_esp32 == True:
        sig = args.flash_max
    else:
        sig = np.mean(sizes)
    scaled_scores = scaled_scores * (sizes / np.power(sig, 2.)) * \
        np.exp(-np.power(sizes, 2.) / (2 * np.power(sig, 2.)))
    corrector = np.max(scores)/np.max(scaled_scores)
elif args.scale_func == 'rayleigh_threshold':
    sig = args.flash_max
    for i in range(len(sizes)):
        if sizes[i] >= sig:
            scaled_scores[i] = scores[i] * (sizes[i] / np.power(sig, 2.)) * \
                np.exp(-np.power(sizes[i], 2.) / (2 * np.power(sig, 2.)))
        else:
          scaled_scores[i] = scores[i]
    corrector = 1

filename_scaled = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_scaled_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
filename_original = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_original_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
filename_accs = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_accs_{args.nasspace}_{args.dataset}_{args.trainval}.npy'
filename_sizes = f'{args.npy_loc}/{args.scale_func}_{args.flash_max}MB_{args.save_string}_{result_search_space}_sizes_{args.nasspace}_{args.dataset}_{args.trainval}.npy'

np.save(filename_scaled, scaled_scores)
np.save(filename_original, scores)
np.save(filename_accs, accs)
np.save(filename_sizes, sizes)

# *************************************
# *************************************
# *************************************

if args.nasspace == 'nasbench101':
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
else:
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))


def scale(x):
    return 2.**(10*x) - 1.

if args.score == 'svd':
    def score_scale(x): return 10.0**x
else:
    def score_scale(x): return x

if args.nasspace == 'nonetwork':
    ax.scatter(scale(accs/100.), score_scale(scores),
               c=newcmp(accs/100., depths))
else:
    if args.plot_size == '0':
        ax.scatter(scale(accs/100. if args.nasspace == 'nasbench201' or 'nds' in args.nasspace else accs),
                   score_scale(scores), c=newcmp(accs/100. if args.nasspace == 'nasbench201' or 'nds' in args.nasspace else accs))
    elif args.plot_size == '2':
        ax.scatter(sizes, scale(accs/100. if args.nasspace ==
                   'nasbench201' or 'nds' in args.nasspace else accs))
    else:
        ax.scatter(sizes, score_scale(scores),
                   c='darkcyan', marker='x', label='Scores')
        # corrector = np.max(scores)/np.max(scaled_scores)
        ax.scatter(sizes, (score_scale(scaled_scores) * corrector), c='coral',
                   marker='.', label=f'Score scaled with {args.scale_func}')

if args.plot_size == '0':
    if args.dataset == 'cifar100':
        ax.set_xticks([scale(float(a)/100.) for a in [40, 60, 70]])
        ax.set_xticklabels([f'{a}' for a in [40, 60, 70]])
    elif args.dataset == 'imagenette2':
        ax.set_xticks([scale(float(a)/100.) for a in [40, 50, 60, 70]])
        ax.set_xticklabels([f'{a}' for a in [40, 50, 60, 70]])
    elif args.dataset == 'ImageNet16-120':
        ax.set_xticks([scale(float(a)/100.) for a in [20, 30, 40, 45]])
        ax.set_xticklabels([f'{a}' for a in [20, 30, 40, 45]])
    elif args.nasspace == 'nasbench101' and args.dataset == 'cifar10':
        ax.set_xticks([scale(float(a)/100.) for a in [50, 80, 90, 95]])
        ax.set_xticklabels([f'{a}' for a in [50, 80, 90, 95]])
    elif args.nasspace == 'nasbench201' and args.dataset == 'cifar10' and args.score == 'svd':
        ax.set_xticks([scale(float(a)/100.) for a in [50, 80, 90, 95]])
        ax.set_xticklabels([f'{a}' for a in [50, 80, 90, 95]])
    elif 'nds_resne' in args.nasspace and args.dataset == 'cifar10':
        ax.set_xticks([scale(float(a)/100.) for a in [85, 88, 91, 94]])
        ax.set_xticklabels([f'{a}' for a in [85, 88, 91, 94]])
    elif args.nasspace == 'nds_darts' and args.dataset == 'cifar10':
        ax.set_xticks([scale(float(a)/100.) for a in [80, 85, 90, 95]])
        ax.set_xticklabels([f'{a}' for a in [80, 85, 90, 95]])
    elif args.nasspace == 'nds_pnas' and args.dataset == 'cifar10':
        ax.set_xticks([scale(float(a)/100.) for a in [90., 91.5, 93, 94.5]])
        ax.set_xticklabels([f'{a}' for a in [90., 91.5, 93, 94.5]])
    else:
        ax.set_xticks([scale(float(a)/100.) for a in [50, 80, 90]])
        ax.set_xticklabels([f'{a}' for a in [50, 80, 90]])
elif args.plot_size == '2':
    if args.dataset == 'cifar100':
        ax.set_yticks([scale(float(a)/100.) for a in [40, 60, 70]])
        ax.set_yticklabels([f'{a}' for a in [40, 60, 70]])
    elif args.dataset == 'imagenette2':
        ax.set_yticks([scale(float(a)/100.) for a in [40, 50, 60, 70]])
        ax.set_yticklabels([f'{a}' for a in [40, 50, 60, 70]])
    elif args.dataset == 'ImageNet16-120':
        ax.set_yticks([scale(float(a)/100.) for a in [20, 30, 40, 45]])
        ax.set_yticklabels([f'{a}' for a in [20, 30, 40, 45]])
    elif args.nasspace == 'nasbench101' and args.dataset == 'cifar10':
        ax.set_yticks([scale(float(a)/100.) for a in [50, 80, 90, 95]])
        ax.set_yticklabels([f'{a}' for a in [50, 80, 90, 95]])
    elif args.nasspace == 'nasbench201' and args.dataset == 'cifar10' and args.score == 'svd':
        ax.set_yticks([scale(float(a)/100.) for a in [50, 80, 90, 95]])
        ax.set_yticklabels([f'{a}' for a in [50, 80, 90, 95]])
    elif 'nds_resne' in args.nasspace and args.dataset == 'cifar10':
        ax.set_yticks([scale(float(a)/100.) for a in [85, 88, 91, 94]])
        ax.set_yticklabels([f'{a}' for a in [85, 88, 91, 94]])
    elif args.nasspace == 'nds_darts' and args.dataset == 'cifar10':
        ax.set_yticks([scale(float(a)/100.) for a in [80, 85, 90, 95]])
        ax.set_yticklabels([f'{a}' for a in [80, 85, 90, 95]])
    elif args.nasspace == 'nds_pnas' and args.dataset == 'cifar10':
        ax.set_yticks([scale(float(a)/100.) for a in [90., 91.5, 93, 94.5]])
        ax.set_yticklabels([f'{a}' for a in [90., 91.5, 93, 94.5]])
    else:
        ax.set_yticks([scale(float(a)/100.) for a in [50, 80, 90]])
        ax.set_yticklabels([f'{a}' for a in [50, 80, 90]])
ax.spines["top"].set_visible(False)
ax.spines["right"].set_visible(False)

nasspacenames = {
    'nds_resnext-a_in': 'NDS-ResNeXt-A(ImageNet)',
    'nds_resnext-b_in': 'NDS-ResNeXt-B(ImageNet)',
    'nds_resnext-a': 'NDS-ResNeXt-A(CIFAR10)',
    'nds_resnext-b': 'NDS-ResNeXt-B(CIFAR10)',
    'nds_nasnet': 'NDS-NASNet(CIFAR10)',
    'nds_nasnet_in': 'NDS-NASNet(ImageNet)',
    'nds_enas': 'NDS-ENAS(CIFAR10)',
    'nds_enas_in': 'NDS-ENAS(ImageNet)',
    'nds_amoeba': 'NDS-Amoeba(CIFAR10)',
    'nds_amoeba_in': 'NDS-Amoeba(ImageNet)',
    'nds_resnet': 'NDS-ResNet(CIFAR10)',
    'nds_pnas': 'NDS-PNAS(CIFAR10)',
    'nds_pnas_in': 'NDS-PNAS(ImageNet)',
    'nds_darts': 'NDS-DARTS(CIFAR10)',
    'nds_darts_in': 'NDS-DARTS(ImageNet)',
    'nds_darts_fix-w-d': 'NDS-DARTS fixed width/depth (CIFAR10)',
    'nds_darts_in_fix-w-d': 'NDS-DARTS fixed width/depth (ImageNet)',
    'nds_darts_in': 'NDS-DARTS(ImageNet)',
    'nasbench101': 'NAS-Bench-101',
    'nasbench201': 'NAS-Bench-201'
}

if args.plot_size == '2':
    ax.set_ylabel(f'{"Test" if not args.trainval else "Validation"} accuracy')
else:
    ax.set_ylabel('Score')

if args.plot_size == '0':
    file_begining_name = 'accs'
    ax.set_xlabel(f'{"Test" if not args.trainval else "Validation"} accuracy')
elif args.plot_size == '2':
    file_begining_name = 'AS'
    ax.set_xlabel('Size (MB)')
else:
    file_begining_name = 'sizes'
    ax.set_xlabel('Size (MB)')

if args.scale_func != '':
    ax.set_title(
        f'{nasspacenames[args.nasspace]} {args.dataset} \n $\\tau=${tau:.3f} $\\sigma=${args.flash_max} MB \n Scaled with {args.scale_func} function')
    filename = f'{args.save_loc}/{args.scale_func}_sigma{args.flash_max}MB_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
else:
    filename = f'{args.save_loc}/{args.scale_func}_{file_begining_name}_{result_search_space}_{args.save_string}_{args.score}_{args.nasspace}_{args.dataset}{"_" + args.init + "_" if args.init != "" else args.init}{"_dropout" if args.dropout else ""}_{args.augtype}_{args.sigma}_{args.repeat}_{args.trainval}_{args.batch_size}_{args.maxofn}_{args.seed}'
    ax.set_title(
        f'{nasspacenames[args.nasspace]} {args.dataset} \n $\\tau=${tau:.3f}')

ax.legend()

plt.tight_layout()
# plt.savefig(filename + '.pdf')
print(filename)
plt.savefig(filename + '.png')
